$(function() {
    // Global variables.
    let $loginForm = $('#login form');
    let $registerForm = $('#register form');
    let $toggle = $('.toggle');
    let $error = $('.error');
    let $header = $('h1:not("#header h1")');
    let loginText = 'Login';
    let registerText = 'Register';
    let loginPromptText = 'Already have an account?';
    let registerPromptText = 'Need to register a new account?';
   
    // Takes user to recipe browsing page.
    function redirect(data) {
        window.location.href = '../recipe';
    }
    
    // Attaches handler to login form submission.
    // Tries to log user in.
    // If successful, redirects to recipe browsing page.
    // If unsuccessful, shows error message.
    $loginForm.on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '../login/',
            data: $loginForm.serialize(),
            success: redirect,
            error: function(data) {
                $error.text('Error: Improper username or password.');
            }
        });
    });
   
    // Attaches handler to registration form submission.
    // Tries to register a new user.
    // If successful, redirects to recipe browsing page.
    // If unsuccessful, shows appropriate error message.
    // Unsuccessful registration can be because of either:
    // 1. Choosing an already-existing username
    // 2. Passwords not matching
    $registerForm.on('submit', function(e) {
        e.preventDefault();
        
        registerData = $registerForm.serialize();
        
        $.ajax({
            type: 'POST',
            url: '../register/',
            data: registerData,
            success: redirect,
            error: function(data) {
                data = data.responseJSON;
                $error.text(`Error: ${data.errors[0]}`);
            }
        });
        
    });
    
    // Attaches handler to toggle link click.
    // Changes the visible form between login and register.
    // Removes any data from forms and removes error messages.
    $toggle.on('click', function(e) {
        e.preventDefault();
        
        // Hide current form, show other form.
        $registerForm.parent().toggle();
        $loginForm.parent().toggle();
        
        // Change header and link text
        this.text = this.text === registerPromptText ? loginPromptText : registerPromptText;
        $header.text($header.text() === registerText ? loginText : registerText);
        
        // Reset form
        $error.text('');
        $('input[type="text"]').val('');
        $('input[type="password"]').val('');
    });
    
    // Start page with login form showing.
    $registerForm.parent().toggle();
    $header.text(loginText);
    $toggle.text(registerPromptText);
});