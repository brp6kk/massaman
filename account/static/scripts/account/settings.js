$(function() {
    // Global variables.
    let $sidebarLinks = $('#sidebar ul.links li');
    let $accountScreen = $('.account');
    let $passwordScreen = $('.password');
    let $deleteScreen = $('.delete');
    let $accountForm = $('.account form');
    let $passwordForm = $('.password form');
    let $deleteForm = $('.delete form');
    let $error = $('.error');
    let $success = $('.success');

    $sidebarLinks.on('click', function(e) {
        e.preventDefault();
        // Stop animations currently in progress.
        // Caused by user clicking links very quickly.
        $accountScreen.stop();
        $passwordScreen.stop();
        $deleteScreen.stop();

        $error.text('');
        $success.text('');

        let $toShow, $toHide;
        if ($(e.target).is(':contains(Delete)')) {
            $toShow = $deleteScreen;
            $toHide = [$passwordScreen, $accountScreen];
        } else if ($(e.target).is(':contains(Password)')) {
            $toShow = $passwordScreen;
            $toHide = [$accountScreen, $deleteScreen];
        } else { // Account settings
            $toShow = $accountScreen;
            $toHide = [$passwordScreen, $deleteScreen];
        }

        $toHide.forEach(function($elem) {
            if ($elem.is(':visible')) {
                $elem.fadeOut();
            }
        })

        if ($toShow.is(':hidden')) {
            $toShow.delay(700).fadeIn();
        }
    });
    $passwordScreen.hide();
    $deleteScreen.hide();
   
    // Attaches handler to general account settings form submission.
    // Tries to update settings.
    // If successful, shows confirmation message.
    // If unsuccessful, shows error message.
    $accountForm.on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '../change_settings/',
            data: $accountForm.serialize(),
            success: function(data) {
                flashText($success, 'Settings saved.');
            },
            error: function(data) {
                flashText($error, 'Error: Username already taken.');
            }
        });
    });
    
    // Attaches handler to password change form submission.
    // Tries to change password.
    // If successful, shows confirmation message.
    // If unsuccessful, shows error message.
    $passwordForm.on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '../change_password/',
            data: $passwordForm.serialize(),
            success: function(data) {
                flashText($success, 'New password set.');
            },
            error: function(data) {
                data = data.responseJSON;
                flashText($error, `Error: ${data.errors[0]}`);
            }
        });
    });

    // Attaches handler to delete account form submission.
    // Tries to delete account, which occurs only if
    // the passwords match and are that of the user.
    // If successful, the user is logged out 
    // and redirected to the landing page.
    // If unsuccessful, shows error message.
    $deleteForm.on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '../delete_account/',
            data: $deleteForm.serialize(),
            success: function(data) {
                $success.text('Account deleted. You will be redirected shortly.');
                setTimeout(redirect, 2000);
            },
            error: function(data) {
                flashText($error, 'Error: Improper password.');
            }
        });
    });

    // Take user to landing page.
    function redirect() {
        window.location.href = '../';
    }

    // Show text for specified number of milliseconds.
    function flashText($object, text, milliseconds=5000) {
        $object.text(text);
        setTimeout(() => $object.text(''), milliseconds);
    }
});