from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST

from .forms import DeleteAccountForm, LoginForm, RegisterForm, SettingsForm

def landing(request):
    """Default landing page for Massaman."""
    if request.user.is_authenticated:
        return redirect('recipe:browse_all')
    return render(request, 'account/landing.html')

def authenticate_view(request):
    """Set up necessary forms for authentication page."""
    # Authenticated users shouldn't log in again.
    if request.user.is_authenticated:
        return redirect('recipe:browse_all')
    
    login_form = LoginForm()
    register_form = RegisterForm()
    context = {
        'login_form': login_form,
        'register_form': register_form
    }
    return render(request, 'account/authenticate.html', context)

@login_required
def settings_view(request):
    """Set up necessary forms for settings page."""
    settings_form = SettingsForm(instance=request.user)
    password_change_form = PasswordChangeForm(request.user)
    delete_account_form = DeleteAccountForm(request.user)

    context = {'settings_form': settings_form, 
               'password_change_form': password_change_form,
               'delete_account_form': delete_account_form}
    return render(request, 'account/settings.html', context)

@require_POST
def login_submit(request):
    """Background process: Try to log a user in."""
    form = LoginForm(data=request.POST)
    if form.is_valid():
        data = form.cleaned_data
        user = authenticate(request, username=data['username'], password=data['password'])
        if user is None:
            # Account does not exist.
            return JsonResponse({}, status=500)
        login(request, user)
        return JsonResponse({})
    
    # Improper login form.
    return JsonResponse({}, status=500)

@require_POST
def register_submit(request):
    """Background process: Try to register a new user."""
    form = RegisterForm(data=request.POST)
    if form.is_valid():
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password1'])
        user.save()
        login(request, user)
        return JsonResponse({})

    # Improper register form.
    error_dict = get_error_dict(form)
    return JsonResponse(error_dict, status=500)

@login_required
@require_POST
def settings_submit(request):
    """Background process: Try to update an existing user's
       username, first name, and last name."""
    user_form = SettingsForm(instance=request.user, data=request.POST)
    if user_form.is_valid():
        user_form.save()
        return JsonResponse({})

    return JsonResponse({}, status=500)

@login_required
@require_POST
def password_change_submit(request):
    """Background process: Try to change a user's password."""
    password_form = PasswordChangeForm(request.user, data=request.POST)
    if password_form.is_valid():
        user = password_form.save()
        update_session_auth_hash(request, user)
        return JsonResponse({})
    else:
        error_dict = get_error_dict(password_form)
        return JsonResponse(error_dict, status=500)

@login_required
@require_POST
def delete_account(request):
    """Background process: Try to delete a user's account."""
    delete_account_form = DeleteAccountForm(user=request.user, data=request.POST)
    if delete_account_form.is_valid():
        to_delete = request.user
        logout(request)
        to_delete.delete()
        return JsonResponse({})
    return JsonResponse({}, status=500)

def logout_view(request):
    """Log a user out."""
    logout(request)
    return redirect('account:landing')

def get_error_dict(form):
    """Creates a flattened list of errors, 
       then sets the list as the value corresponding 
       to key 'errors'."""
    errors = list(dict(form.errors).values())
    errors_flattened = [er for error in errors for er in error]
    return {'errors': errors_flattened}