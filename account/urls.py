from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'account'
urlpatterns = [
    path("", views.landing, name="landing"),
    path("authenticate/", views.authenticate_view, name="authenticate"),
    path("settings/", views.settings_view, name="settings"),
    path("logout/", views.logout_view, name="logout"),
    
    # Background processes
    path("login/", views.login_submit, name="login"),
    path("register/", views.register_submit, name="register"),
    path("change_settings/", views.settings_submit, name="change_settings"),
    path("change_password/", views.password_change_submit, name="change_password"),
    path("delete_account/", views.delete_account, name="delete_account"),
]