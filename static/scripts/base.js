let csrftoken = Cookies.get('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
    
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(function() {
    // Sidebar visibility is dependent on whether or not
    // there's actually any content within it.
    let $sidebar = $('#sidebar');
    let $content = $('#content');
    if ($sidebar.children().length > 0) {
        $content.addClass('offset');
    } else {
        $sidebar.hide();
    }

    // Header menus
    let $settingsButton = $('.buttons a.settings');
    let $settingsMenu = $('.menu.settings');
    let $generalButton = $('.buttons a.general');
    let $generalMenu = $('.menu.general');
    $settingsButton.on('click', function(e) {
        e.preventDefault();
        if ($generalMenu.is(":visible")) {
            $generalMenu.slideToggle();
        }
        $settingsMenu.slideToggle();
    });
    $generalButton.on('click', function(e) {
        e.preventDefault();
        if ($settingsMenu.is(':visible')) {
            $settingsMenu.slideToggle();
        }
        $generalMenu.slideToggle();
    });
    $generalMenu.hide();
    $settingsMenu.hide();
});

function changeCheckboxes() {
    // Loops through all labels.
    // If the label is for a "DELETE",
    // change the label to an "X".
    $('label').each(function() {
        let use = $(this).attr('for');
        if (use.includes('DELETE')) {
            $(this).text('X');
        } else if ($(this).text() == 'Id:') {
            $(this).hide();
        }
    });

    $(':checkbox').hide();
}

// Return respective string month name
// from an integer, where 1 = January
function monthName(num) {
    let result;
    switch(num) {
        case 1:
            result = "January";
            break;
        case 2:
            result = "February";
            break;
        case 3:
            result = "March";
            break;
        case 4:
            result = "April";
            break;
        case 5:
            result = "May";
            break;
        case 6:
            result = "June";
            break;
        case 7:
            result = "July";
            break;
        case 8:
            result = "August";
            break;
        case 9:
            result = "September";
            break;
        case 10:
            result = "October";
            break;
        case 11:
            result = "November";
            break;
        case 12:
            result = "December";
            break;
        default:
            result = "";
            break;
    }
      
    return result;
}