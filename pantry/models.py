from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

class Ingredient(models.Model):
    """Model representing an ingredient."""
    name = models.CharField(max_length=100)
    quantity = models.CharField(max_length=100)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, limit_choices_to={'model__in': ('recipe', 'meal',)})
    object_id = models.PositiveIntegerField()
    owner = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.name + " (" + self.quantity + ")"