from django.forms import modelformset_factory

from .models import Ingredient

IngredientFormSet = modelformset_factory(Ingredient, 
        fields=["name", "quantity"], extra=1, can_delete=True)