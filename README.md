**Massaman** is a recipe-sharing web app. Anyone can browse public recipes, and authenticated users can create their own recipes.

**[Staging for version 1.0.0 is currently live!](https://massaman-staging.herokuapp.com/)**
(Note, though, that user-uploaded media currently is not saved. Don't worry, there's [an open issue](https://gitlab.com/brp6kk/massaman/-/issues/5) for that!)

Please see [this project's wiki page](https://gitlab.com/brp6kk/massaman/-/wikis/home) for additional information.