from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from pantry.models import Ingredient

class Meal(models.Model):
    """Model representing a meal."""
    RATING_CHOICES = (
        (0, 'none'),
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    owner = models.ForeignKey(User, related_name='user_meals', on_delete=models.CASCADE)
    when = models.DateTimeField()
    recipes = models.ManyToManyField('recipe.Recipe', related_name='recipe_meals', blank=True)
    rating = models.PositiveIntegerField(choices=RATING_CHOICES, default=0)
    comments = models.TextField(blank=True)

    foods = GenericRelation(Ingredient)

    class Meta:
        ordering = ('when',)
    