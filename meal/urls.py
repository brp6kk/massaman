from django.urls import path
from . import views

app_name = 'meal'
urlpatterns = [
    path("", views.meal_monthly, name="meal_monthly"),
    path("<int:year>/<int:month>/", views.meal_monthly, name="meal_monthly"),
    path("<int:year>/<int:month>/list/", views.meal_monthly_list, name="meal_monthly_list"),
    path("<int:year>/<int:month>/<int:day>/", views.meal_daily, name="meal_daily"),
    path("create/", views.meal_create, name="meal_create"),
    path("edit/<int:mealid>/", views.meal_create, name="meal_edit"),

    # Backend
    path("submit/", views.meal_submit, name="meal_submit"),
    path("delete/", views.meal_delete, name="meal_delete"),
    path("get_monthly/", views.get_monthly, name='get_monthly'),
]