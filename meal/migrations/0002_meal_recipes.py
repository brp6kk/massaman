# Generated by Django 3.1.4 on 2021-03-21 17:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0004_auto_20210110_1556'),
        ('meal', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='meal',
            name='recipes',
            field=models.ManyToManyField(related_name='meals', to='recipe.Recipe'),
        ),
    ]
