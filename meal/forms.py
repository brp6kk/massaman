from django import forms

from .models import Meal
from pantry.models import Ingredient
from recipe.models import Recipe

class MealForm(forms.ModelForm):
    class Meta:
        model = Meal
        fields = ('when', 'comments', 'recipes')
    
    def __init__(self, user=None, *args, **kwargs):
        super(MealForm, self).__init__(*args, **kwargs)

        if user:
            # Only show recipes accessible to a user
            self.fields['recipes'] = forms.ModelMultipleChoiceField(queryset=Recipe.accessible.for_user(user), required=False)
        