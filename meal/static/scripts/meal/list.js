$(function() {
    // Attaches handler to yes button click.
    // Sends request to delete recipe from database.
    $('.yes').on('click', function(e){
        let mealid = $(this).parent().parent().attr('data-mid');

        $.ajax({
            type: 'POST',
            url: '/meal/delete/',
            data: {'mealid': mealid},
            success: function(data) {
                window.location.href = '/meal/';
            }
        });
    });

    // Attaches handler to no button click.
    // Hides delete prompt.
    $('.no').on('click', function(e){
        $deletePrompt = $(this).parent().detach();
    });

    let $deletePrompt = $('.delete:last').detach();

    // Attaches handler to edit button click.
    // Determines appropriate page and redirects.
    $('.edit').on('click', function(e) {
        let mealid = $(this).parent().parent().attr('data-mid');
        let path = "/meal/edit/" + mealid;
        window.location.href = path;
    });

    // Attaches handler to delete button click.
    // Show delete prompt.
    $('.delete').on('click', function(e) {
        $(this).parent().after($deletePrompt);
    });
});