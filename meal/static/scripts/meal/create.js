$(function() {
    changeCheckboxes();
    $('#id_when').datetimepicker({format: 'm/d/Y H:i'});

    let $mealForm = $('#main form');
    let $foodForm = $('#foods form');
    let $emptyFoodForm = $('#foods_empty').detach();

    // Clear recipes box.
    $('#main button').on('click', function(e) {
        e.preventDefault();
        let $recipeSelect = $('#id_recipes');
        $recipeSelect[0].selectedIndex = -1;
    });

    // Add a new blank food form to the bottom of the foods formset.
    $('#foods button').on('click', function(e) {
        e.preventDefault();

        let $idFoodForm = $('#id_form-TOTAL_FORMS');
        let newIndex = $idFoodForm.val();
        $foodForm.append($emptyFoodForm.html().replace(/__prefix__/g, newIndex));
        $idFoodForm.val(parseInt(newIndex) + 1);

        $('#id_form-' + newIndex + '-name').focus();
    });

    // Submit meal data.
    $('.submit form').on('submit', function(e){
        e.preventDefault();

        // NOTE - meal time will be saved as central time.
        let mealData = $mealForm.serialize();
        let foodData = $foodForm.serialize();
        let ajaxData = {
            'mealData': mealData,
            'foodData': foodData
        }

        let mealid = getMealId();
        if (mealid) {
            ajaxData['mealid'] = mealid;
        }

        $.ajax({
            type: 'POST',
            url: '/meal/submit/',
            data: ajaxData,
            success: function(data) {
                // go to daily meal page on success
                window.location.href = data['redirecturl'];
            },
            error: function(data) {
                $('.error').text('Error: Some required fields not filled out!');
            }
        });
    });

    // Gets the mealid from the page's path,
    // or returns null if the mealid doesn't exist.
    function getMealId() {
        let paths = window.location.pathname.split("/");
        let mealid = parseInt(paths[paths.length - 2]);

        // return null rather than NaN
        if (mealid === NaN) {
            return null;
        }

        return mealid;
    }

    // Attaches handler to checkboxes.
    // Hide the parent when change event occurs.
    $('form').on('change', ':checkbox', function(e) {
        $(this).parent().hide();
    });

    // Change header text if editing a meal
    // rather than creating a new one.
    if (getMealId()) {
        $('.meal h1').text("Edit Meal");
    }
});