$(function() {
    let month;
    let year;
    let $mainCalendar = $('.main');
    
    staticCalendarSetup();
    $.ajax({
        type: 'POST',
        url: '/meal/get_monthly/',
        data: {
            'month': month,
            'year': year
        },
        success: dynamicCalendarSetup,
        error: function(data) {
            console.log("Failure");
        }
    });

    // Set up the appearance of the calendar
    // based on the month/year that is to be displayed.
    function staticCalendarSetup() {
        // Note - for Date objects, month 0 = January
        // but for me, month 1 = January.
        // This is why there are various + 1 and -1s 
        // included when dealing with setting/getting months.

        let currentPage = window.location.href.split('/');
        let currentDate = new Date();
        let linkStart;

        if (currentPage[currentPage.length-2] === 'meal') {
            // No month specified, use this month.
            month = currentDate.getMonth() + 1;
            year = currentDate.getFullYear();
            linkStart = '../meal/'
        } else {
            // Month & year specified.
            month = parseInt(currentPage[currentPage.length-2]);
            year = parseInt(currentPage[currentPage.length-3]);
            currentDate.setMonth(month-1);
            currentDate.setFullYear(year);
            linkStart = '../../../meal/'
        }

        // Remove blank li elements to position
        // days of week appropriately.
        currentDate.setDate(1);
        let weekday = currentDate.getDay();
        for (let i = 0; i < (6 - weekday); i++) {
            $mainCalendar.children(':first').remove();
        }

        // Remove ending li elements based on
        // number of days in this month.
        let numToRemove = 31 - daysInMonth(month, year);
        for (let i = 0; i < numToRemove; i++) {
            $mainCalendar.children(':last').remove();
        }

        let $prev = $('#prev_month');
        let $this = $('#this_month');
        let $next = $('#next_month');

        // This month text.
        let monthStr = monthName(month);
        let thisStr = monthStr + " " + year;
        let thisLink = "/meal/" + year + "/" + month + "/list/";
        $this.text(thisStr);
        $this.attr('href', thisLink);


        // Previous month link.
        let prevDate = new Date(currentDate.valueOf());
        prevDate.setDate(0);
        let prevMonth = prevDate.getMonth() + 1;
        let prevYear = prevDate.getFullYear();
        let prevLink = linkStart + prevYear + "/" + prevMonth;
        $prev.attr('href', prevLink);

        // Next month link.
        let nextDate = new Date(currentDate.valueOf());
        nextDate.setMonth(nextDate.getMonth() + 1, 1);
        let nextMonth = nextDate.getMonth() + 1;
        let nextYear = nextDate.getFullYear();
        let nextLink = linkStart + nextYear + "/" + nextMonth;
        $next.attr('href', nextLink);
    }

    // Set up the dynamic aspect of the calender,
    // changing the class of dates with at least one meal
    // and adding tooltips to show the times of these meals.
    function dynamicCalendarSetup(data) {
        let meals = data['meals'];

        for (let i = 0; i < meals.length; i++) {
            let m = meals[i];

            let dateObj = new Date(year, month-1, m['day'],m['hour'], m['minute']);
            let time = dateObj.toLocaleTimeString();
            let $mealElement = $('li').filter(function() {
                return $(this).text() == m['day']; // get element corresponding to day of meal.
            });

            // Change formatting and add time of this meal
            // to tooltip displaying all times.
            $mealElement.addClass('active');
            let currentMeals = $mealElement.attr('title');
            if (currentMeals) {
                $mealElement.attr('title', currentMeals + "\n" + time);
            } else {
                $mealElement.attr('title', time);
            }
        }

        // Add tooltips and set tooltips to move
        // with user's cursor.
        $( document ).tooltip({
            track: true,
        });
    }

    $('li').on('click', function(e){
        let day = $(this).text();
        day = parseInt(day);
        if (!isNaN(day)) {
            let href = '/meal/' + year + '/' + month + '/' + day;
            window.location.href = href;
        }
    });

    // Return the number of days in a month from
    // integer month and year, where 1 = January
    function daysInMonth(month, year) {
        // https://stackoverflow.com/a/1184359
        return new Date(year, month, 0).getDate();
    }
});