from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import Http404, JsonResponse, QueryDict
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.views.decorators.http import require_POST

import datetime
import json 

from .forms import MealForm
from .models import Meal
from pantry.forms import IngredientFormSet
from pantry.models import Ingredient

@login_required
def meal_create(request, mealid=None):
    """Page to allow recipe editing or creation.
    """
    if mealid:
        meal = get_object_or_404(Meal, id=mealid)
        if meal.owner != request.user:
            raise Http404("Unable to access meal")
        meal_form = MealForm(request.user, instance=meal)
        food_form = IngredientFormSet(queryset=meal.foods.all())
    else: 
        meal_form = MealForm(request.user)
        food_form = IngredientFormSet(queryset=Ingredient.objects.none())

    return render(request, 'meal/create.html', {'meal_form': meal_form, 'food_form': food_form, 'mealid': mealid})

@login_required
@require_POST
def meal_submit(request):
    """Backend process to save a meal and
       its related foods."""
    meal_data = QueryDict(request.POST['mealData'])
    food_data = QueryDict(request.POST['foodData'])

    try:
        # Editing a meal
        mealid = request.POST['mealid']
        meal = get_object_or_404(Meal, id=mealid)
        if meal.owner != request.user:
            raise Http404("Unable to access meal")
        meal_form = MealForm(request.user, instance=meal, data=meal_data)
    except:
        # Creating a meal
        meal_form = MealForm(request.user, data=meal_data)

    # Save meal
    if meal_form.is_valid():
        meal = meal_form.save(commit=False)
        meal.owner = request.user
        meal.save()
        meal_form.save_m2m()

        # Save valid ingredients
        food_form = IngredientFormSet(food_data)
        if food_form.is_valid():
            food = food_form.save(commit=False)
            for val in food:
                val.owner = meal
                val.save()

            redirecturl = '/meal/' + str(meal.when.year) + '/' + str(meal.when.month) + '/' + str(meal.when.day)
            
            return JsonResponse({'redirecturl': redirecturl})
    
    return JsonResponse({}, status=500)

@login_required
@require_POST
def meal_delete(request):
    """Backend process to delete a meal."""
    mealid = request.POST['mealid']
    meal = get_object_or_404(Meal, id=mealid)
    if meal.owner != request.user:
        raise Http404("Unable to access meal")
    
    meal.delete()

    return JsonResponse({})

@login_required
def meal_monthly(request, year=None, month=None):
    """Page to show a calendar view
       with summary of meals.
       Note that actual meal information is obtained
       via a POST request on page load so that JS
       can be used to create a dynamic calendar.
    """
    return render(request, 'meal/month.html')

@login_required
@require_POST
def get_monthly(request):
    """Backend process to get the meals
       for a particular month of the year.
    """
    month = request.POST['month']
    year = request.POST['year']
    query = Q(owner=request.user) & Q(when__month=month) & Q(when__year=year)
    meals = Meal.objects.filter(query)

    meals_js = list()
    for meal in meals:
        obj = dict()
        obj['day'] = meal.when.day
        obj['hour'] = meal.when.hour
        obj['minute'] = meal.when.minute
        meals_js.append(obj)
    
    return JsonResponse({'meals': meals_js})

@login_required
def meal_daily(request, year, month, day):
    query = Q(owner=request.user) & Q(when__day=day) & Q(when__month=month) & Q(when__year=year)
    meals = Meal.objects.filter(query)

    return render(request, 'meal/day.html', {'meals': meals})

@login_required
def meal_monthly_list(request, year, month):
    query = Q(owner=request.user) & Q(when__month=month) & Q(when__year=year)
    meals = Meal.objects.filter(query)

    return render(request, 'meal/month_list.html', {'meals': meals, 'month': month, 'year': year})