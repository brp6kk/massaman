from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils.text import slugify

from taggit.managers import TaggableManager

from .fields import OrderField
from meal.models import Meal
from pantry.models import Ingredient

class PublicManager(models.Manager):
    """Manager to get queryset consisting of public recipes."""
    def get_queryset(self):
        return super(PublicManager, self).get_queryset().filter(status='public')

class AccessibleManager(models.Manager):
    """Manager to get queryset of accessible recipes,
       which includes public recipes and 
       the recipes created by a logged-in user."""
    def for_user(self, user):
        query = Q(status='public')
        publicOnly = self.get_queryset().filter(query)
        if user.is_anonymous:
            return publicOnly
        query = query | Q(owner=user)
        own = self.get_queryset().filter(query)
        return own

class Recipe(models.Model):
    """Model representing a recipe."""
    STATUS_CHOICES = (
        ('public', 'Public'),
        ('private', 'Private'),
    )
    owner = models.ForeignKey(User, related_name='user_recipes', on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=100)
    status = models.CharField(max_length=7, choices=STATUS_CHOICES, default='public')
    servings = models.PositiveIntegerField()
    make_time = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    tags = TaggableManager(blank=True)
    picture = models.ImageField(blank=True, upload_to='images/%Y/%m/%d/')
    url = models.URLField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    slug = models.SlugField(max_length=100, unique=True)
    meals = models.ManyToManyField(Meal, related_name='meal_recipes', blank=True)

    ingredients = GenericRelation(Ingredient)

    objects = models.Manager()
    public = PublicManager()
    accessible = AccessibleManager()

    class Meta:
        ordering = ('-updated',)
    
    def __str__(self):
        return self.title

    # https://kodnito.com/posts/slugify-urls-django/
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Recipe, self).save(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse('recipe:recipe_detail', args=[self.owner.username, self.slug])

class Instruction(models.Model):
    """Model representing the instruction of a recipe."""
    instruction = models.TextField()
    recipe = models.ForeignKey(Recipe, related_name='instructions', on_delete=models.CASCADE)
    order = OrderField(blank=True, for_fields=['recipe'])

    def __str__(self):
        result = self.instruction
        if len(result) > 30:
            result = result[:30] + "..."
        return str(self.order) + ". " + result