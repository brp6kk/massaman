from django.urls import path
from . import views

app_name = 'recipe'
urlpatterns = [
    path("", views.browse_all, name="browse_all"),
    path("mine/", views.browse_mine, name="browse_mine"),
    path("create/", views.recipe_create, name="recipe_create"),
    path("edit/<username>/<slug:recipe>/", views.recipe_create, name="recipe_edit"),

    # Backend
    path("create/submit/", views.recipe_submit, name="recipe_submit"),
    path("edit/<username>/<slug:recipe>/submit/", views.recipe_submit, name="recipe_submit_edit"),
    path("delete/", views.recipe_delete, name="recipe_delete"),

    path("<username>/<slug:recipe>/", views.recipe_detail, name="recipe_detail"),
]