from django.contrib import admin

from .models import Instruction, Recipe

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Instruction)