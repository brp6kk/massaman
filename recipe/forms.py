from django import forms
from django.forms.models import inlineformset_factory

from .models import Recipe, Instruction

InstructionFormSet = inlineformset_factory(Recipe, Instruction, 
        fields=['instruction'], extra=1, can_delete=True)

class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = ('title', 'status', 'servings', 'make_time', 'description', 'tags', 'picture', 'url')