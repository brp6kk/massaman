from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import Http404, JsonResponse, QueryDict
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST

from .forms import RecipeForm, InstructionFormSet
from .models import Recipe
from pantry.forms import IngredientFormSet
from pantry.models import Ingredient

def browse(request, recipe_list, personal_page=False):
    """Get page and recipe for a browsing page."""
    paginator = Paginator(recipe_list, 12)
    page = request.GET.get('page')
    try:
        recipes = paginator.page(page)
    except PageNotAnInteger:
        recipes = paginator.page(1)
    except EmptyPage:
        recipes = paginator.page(paginator.num_pages)
    return render(request, 'recipe/browse.html', {'recipes': recipes, 'page': page, 'personal_page': personal_page})

def browse_all(request):
    """Browsing page for all accessible recipes."""
    recipe_list = Recipe.accessible.for_user(request.user).order_by('-updated')
    return browse(request, recipe_list)
    
@login_required
def browse_mine(request):
    """Browsing page for a user's created recipes."""
    recipe_list = request.user.user_recipes.order_by('-updated')
    return browse(request, recipe_list, True)

def recipe_detail(request, username, recipe):
    """Page showing the details of a recipe."""
    recipe = get_object_or_404(Recipe, slug=recipe, owner__username=username)
    if recipe.status == 'private' and recipe.owner != request.user:
        raise Http404("Unable to access recipe")
    return render(request, 'recipe/recipe_detail.html', {'recipe': recipe})

@login_required
def recipe_create(request, username=None, recipe=None):
    """Page to allow recipe editing or creation."""
    if username and recipe:
        recipe = get_object_or_404(Recipe, slug=recipe, owner__username=username)
        if recipe.owner != request.user:
            raise Http404("Unable to access recipe")
        recipe_form = RecipeForm(instance=recipe)
        ingredient_form = IngredientFormSet(queryset=recipe.ingredients.all())
        instruction_form = InstructionFormSet(instance=recipe)
    else:
        recipe_form = RecipeForm()
        ingredient_form = IngredientFormSet(queryset=Ingredient.objects.none())
        instruction_form = InstructionFormSet()
    
    return render(request, 'recipe/recipe_create.html', {'recipe_form': recipe_form, 'ingredient_form': ingredient_form, 'instruction_form': instruction_form})

@login_required
@require_POST
def recipe_submit(request, username=None, recipe=None):
    """Backend process to save a recipe, its ingredients, and its instructions."""
    recipe_data = QueryDict(request.POST.get('recipeData'))
    ingredient_data = QueryDict(request.POST.get('ingredientData'))
    instruction_data = QueryDict(request.POST.get('instructionData'))
    recipe_image = request.FILES.get('recipeImage')

    # Find recipe, if it already exists
    try:
        recipe = get_object_or_404(Recipe, slug=recipe, owner__username=username)
        if recipe.owner != request.user:
            raise Http404("Unable to access recipe")
        recipe_form = RecipeForm(instance=recipe, data=recipe_data)
    except:
        recipe_form = RecipeForm(recipe_data)

    # Save valid recipe
    if recipe_form.is_valid():
        recipe = recipe_form.save(commit=False)
        recipe.owner = request.user
        recipe.picture = recipe_image
        recipe.save()

        # Save valid instructions
        instruction_form = InstructionFormSet(instance=recipe, data=instruction_data)
        if instruction_form.is_valid():
            instruction_form.save()

        # Save valid ingredients
        ingredient_form = IngredientFormSet(ingredient_data)
        if ingredient_form.is_valid():
            ingredients = ingredient_form.save(commit=False)
            for ingredient in ingredients:
                ingredient.owner = recipe
                ingredient.save()
            
            redirecturl = '/recipe/' + request.user.username + '/' + recipe.slug
            return JsonResponse({'redirecturl': redirecturl})

    return JsonResponse({}, status=500)
    
@login_required
@require_POST
def recipe_delete(request):
    """Backend process to delete a recipe."""
    recipe = get_object_or_404(Recipe, slug=request.POST['slug'], owner__username=request.user)
    recipe.delete()

    return JsonResponse({})