$(function() {

    // Attaches handler to yes button click.
    // Sends request to delete recipe from database.
    $('.yes').on('click', function(e){
        let hrefArray = window.location.href.split('/');
        let slug = hrefArray[hrefArray.length - 2];

        $.ajax({
            type: 'POST',
            url: '../../delete/',
            data: {'slug': slug},
            success: function(data) {
                window.location.href = '../../'
            }
        });
    });

    // Attaches handler to no button click.
    // Hides delete prompt.
    $('.no').on('click', function(e){
        $deletePrompt = $(this).parent().detach();
    });

    let $deletePrompt = $('.delete:last').detach();

    // Attaches handler to edit button click.
    // Determines appropriate page and redirects.
    $('.edit').on('click', function(e) {
        let path = window.location.pathname;
        let endpath = path.substring(7); // remove '/recipe'
        window.location.href = "../../edit" + endpath
    });

    // Attaches handler to delete button click.
    // Show delete prompt.
    $('.delete').on('click', function(e) {
        $(this).parent().after($deletePrompt);
    });
});