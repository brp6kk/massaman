$(function() {
    changeCheckboxes();

    let $recipeForm = $('#main form');
    let $instructionForm = $('#instructions form');
    let $ingredientForm = $('#ingredients form');
    // detached forms to prevent malicious user nonsense
    let $emptyInstructionForm = $('#instructions_empty').detach();
    let $emptyIngredientForm = $('#ingredients_empty').detach();

    // Updates h1 and page title to reflect user's recipe title.
    function updateTitle(newTitle) {
        if (newTitle === "") {
            newTitle = "New Recipe";
        }
        $('.recipe h1').text(newTitle);
        $('title').text(newTitle);
    }

    // Attaches handler to title input form field.
    // Calls updateTitle.
    $('#id_title').on('change', function(e) {
        let newTitle = $(this).val();
        updateTitle(newTitle);
    });

    // Add new blank instruction form to bottom of instructions formset.
    $('#instructions button').on('click', function(e) {
        e.preventDefault();

        let $idInstructionForm = $('#id_instructions-TOTAL_FORMS');
        let newIndex = $idInstructionForm.val();
        $instructionForm.append($emptyInstructionForm.html().replace(/__prefix__/g, newIndex));
        $idInstructionForm.val(parseInt(newIndex) + 1);

        $('#id_instructions-' + newIndex + '-instruction').focus();
    });

    // Add new blank ingredient form to bottom of ingredients formset.
    $('#ingredients button').on('click', function(e) {
        e.preventDefault();

        let $idIngredientForm = $('#id_form-TOTAL_FORMS');
        let newIndex = $idIngredientForm.val();
        $ingredientForm.append($emptyIngredientForm.html().replace(/__prefix__/g, newIndex));
        $idIngredientForm.val(parseInt(newIndex) + 1);

        $('#id_form-' + newIndex + '-name').focus();
    });

    // Submit new recipe, if the forms were correctly filled out.
    $('.submit form').on('submit', function(e) {
        e.preventDefault();

        let recipeData = $recipeForm.serialize();
        let ingredientData = $ingredientForm.serialize();
        let instructionData = $instructionForm.serialize();
        let recipeImage = $('#id_picture')[0].files[0];

        let data = new FormData();
        data.append('recipeData', recipeData);
        data.append('ingredientData', ingredientData);
        data.append('instructionData', instructionData);
        data.append('recipeImage', recipeImage);

        $.ajax({
            type: 'POST',
            url: 'submit/',
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                // go to new recipe page on success
                window.location.href = data['redirecturl']
            },
            error: function(data) {
                $('.error').text('Error: Some required fields not filled out!')
            }
        });
    });

    // Attaches handler to checkboxes.
    // Hide the parent when change event occurs.
    $('form').on('change', ':checkbox', function(e) {
        $(this).parent().hide();
    });
    
    updateTitle($('#id_title').val());
});